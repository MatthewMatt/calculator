import java.math.BigInteger;

import static com.sun.corba.se.impl.util.RepositoryId.cache;
import static jdk.nashorn.internal.objects.Global.Infinity;

public class Calculator {

    BigInteger factorial(int n) {

        BigInteger ret;

        if (n == 0) return BigInteger.ONE;
        if (null != (ret = (BigInteger) cache.get(n))) return ret;
        ret = BigInteger.valueOf(n).multiply(factorial(n-1));
        cache.put(n, ret);
        return ret;

    }

    public static void main(String[] args) {
        System.out.println(new Calculator().factorial(2));
    }


    double addition(double a, double b) {
        return a + b;
    }

    double subtraction(double a, double b) {
        return a - b;
    }

    double multiplication(double a, double b) {
        return a * b;
    }

    double division(double a, double b) {
        if (b == 0) {
            return Infinity;
        }
        return a / b;
    }

}
