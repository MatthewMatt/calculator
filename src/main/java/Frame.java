import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Frame extends JFrame {

    public Frame() {
        super("Calculator");
        setSize(400, 200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        Border border = BorderFactory.createEmptyBorder(5, 5, 5, 5);
        final Calculator calculator = new Calculator();

        JPanel labelPanel = new JPanel();
        labelPanel.setBorder(border);
        labelPanel.setLayout(new BoxLayout(labelPanel, BoxLayout.PAGE_AXIS));

        JLabel label = new JLabel("Введите значения и выберите операцию:");
        JLabel label2 = new JLabel("Факториал высчитывается для первого операнда.:");
        labelPanel.add(label);
        labelPanel.add(label2);
        add(labelPanel, BorderLayout.NORTH);

        JPanel panel = new JPanel();
        final JTextField firstElement = new JTextField("Первый операнд");
        final JTextField secondElement = new JTextField("Второй операнд");
        final JTextField operand = new JTextField("Оператор");
        firstElement.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                firstElement.setText("");
            }
        });
        secondElement.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                secondElement.setText("");
            }
        });
        operand.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                operand.setText("");
            }
        });

        JPanel centralPanel = new JPanel();

        firstElement.setBorder(border);
        secondElement.setBorder(border);
        operand.setBorder(border);
        operand.setEnabled(false);
        operand.setHorizontalAlignment(JTextField.CENTER);
        firstElement.setHorizontalAlignment(JTextField.CENTER);
        secondElement.setHorizontalAlignment(JTextField.CENTER);
        panel.setLayout(new FlowLayout());
        panel.add(firstElement);
        panel.add(operand);
        panel.add(secondElement);


        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new FlowLayout());
        JButton plus = new JButton("+");
        JButton minus = new JButton("-");
        JButton dev = new JButton("/");
        JButton mult = new JButton("*");
        JButton fuctorial = new JButton("!");
        plus.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                operand.setText("+");
            }
        });
        minus.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                operand.setText("-");
            }
        });
        dev.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                operand.setText("/");
            }
        });
        mult.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                operand.setText("*");
            }
        });
        fuctorial.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                operand.setText("!");
            }
        });
        plus.setSize(15, 15);
        minus.setSize(15, 15);
        dev.setSize(15, 15);
        mult.setSize(15, 15);
        fuctorial.setSize(15, 15);
        buttonPanel.add(plus);
        buttonPanel.add(minus);
        buttonPanel.add(dev);
        buttonPanel.add(mult);
        buttonPanel.add(fuctorial);


        centralPanel.add(panel);
        centralPanel.add(buttonPanel);

        add(centralPanel, BorderLayout.CENTER);

        final JPanel resultPanel = new JPanel();
        JButton resultBut = new JButton("=");
        final JTextField result = new JTextField("Результат вычислений");
        result.setBorder(border);
        result.setEnabled(false);
        resultBut.setSize(15, 15);
        resultPanel.add(resultBut);
        resultPanel.add(result);
        add(resultPanel, BorderLayout.SOUTH);

        resultBut.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    String oper = operand.getText().trim();
                    String first = firstElement.getText().trim();
                    if (first.indexOf("0") == 0 && first.length() > 1) {
                        if (!(first.indexOf(".") == 1)) {
                            JOptionPane.showMessageDialog(resultPanel,
                                    "Некорректное значение!",
                                    "Ошибка",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                        if (first.indexOf(".") == 1 && first.length() == 2) {
                            JOptionPane.showMessageDialog(resultPanel,
                                    "Некорректное значение!",
                                    "Ошибка",
                                    JOptionPane.ERROR_MESSAGE);
                        }

                    }
                    if (first.indexOf("-") == 0 && first.indexOf("0") == 1) {
                        if (first.length() == 2) {
                            JOptionPane.showMessageDialog(resultPanel,
                                    "Не стоит перед 0 ставить знак минуса!",
                                    "Ошибка",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                        if (first.indexOf(".") == 2 && first.length() == 3) {
                            JOptionPane.showMessageDialog(resultPanel,
                                    "Некорректное значение!",
                                    "Ошибка",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                    }
                    if (oper.indexOf("!") == 0 && first.indexOf("-") == 0) {
                        JOptionPane.showMessageDialog(resultPanel,
                                "Невозможно посчитать факториал, введите натуральное число!",
                                "Ошибка",
                                JOptionPane.ERROR_MESSAGE);
                    }
                    if (oper.indexOf("!") == 0 && first.indexOf(".") == 1) {
                        JOptionPane.showMessageDialog(resultPanel,
                                "Невозможно посчитать факториал, введите натуральное число!",
                                "Ошибка",
                                JOptionPane.ERROR_MESSAGE);
                    }

                    Double x = Double.parseDouble(first);

                    String second = oper.equals("!") ? "0" : secondElement.getText().trim();
                    if (second.indexOf("0") == 0 && second.length() > 1) {
                        if (!(second.indexOf(".") == 1)) {
                            JOptionPane.showMessageDialog(resultPanel,
                                    "Некорректное значение!",
                                    "Ошибка",
                                    JOptionPane.ERROR_MESSAGE);
                        }

                    }
                    if (second.indexOf("-") == 0 && second.indexOf("0") == 1) {
                        if (second.length() == 2) {
                            JOptionPane.showMessageDialog(resultPanel,
                                    "Так дело не пойдет, не стоит перед 0 ставить знак минуса!",
                                    "Ошибка",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                        if (second.indexOf(".") == 2 && second.length() == 3) {
                            JOptionPane.showMessageDialog(resultPanel,
                                    "Неккоректное значение!",
                                    "Ошибка",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                    }
                    Double y = Double.parseDouble(second);


                    if (oper.equals("+")) {
                        result.setText(String.valueOf(calculator.addition(x, y)));

                    } else if (oper.equals("-")) {
                        result.setText(String.valueOf(calculator.subtraction(x, y)));

                    } else if (oper.equals("*")) {
                        result.setText(String.valueOf(calculator.multiplication(x, y)));

                    } else if (oper.equals("/")) {
                        if(y!=0){
                        result.setText(String.valueOf(calculator.division(x, y)));
                        }else{
                            JOptionPane.showMessageDialog(resultPanel,
                                    "На ноль делить нельзя!",
                                    "Ошибка",
                                    JOptionPane.ERROR_MESSAGE);
                        }

                    } else if (oper.equals("!")) {
                        int z = Math.round(x.floatValue());
                        result.setText(String.valueOf(calculator.factorial(z)));

                    } else {
                        throw new NullPointerException();
                    }
                    Double.parseDouble(first);
                    Double.parseDouble(second);


                } catch (NullPointerException exc) {
                    JOptionPane.showMessageDialog(resultPanel,
                            "Обнаружено пустое значение!",
                            "Ошибка",
                            JOptionPane.ERROR_MESSAGE);
                } catch (NumberFormatException exc) {
                    JOptionPane.showMessageDialog(resultPanel,
                            "Невозможно определить значения!",
                            "Ошибка",
                            JOptionPane.ERROR_MESSAGE);
                } catch (ClassCastException exc) {
                    JOptionPane.showMessageDialog(resultPanel,
                            "Не удалось округлить значение до целочисленного!",
                            "Ошибка",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        });


    }

    public static void main(String[] args) {
        Frame app = new Frame();
        app.setLocationRelativeTo(null);
        app.setVisible(true);
    }
}