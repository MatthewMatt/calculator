import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigInteger;

import static jdk.nashorn.internal.objects.Global.Infinity;

public class CalculatorTest {

    private Calculator calculator;

    @Before
    public void afterTest() {
        calculator = new Calculator();
    }

    @Test
    public void checkAddition() {
        Assert.assertEquals(calculator.addition(6, 3), 9, 0);
    }

    @Test
    public void checkSubtraction() {
        Assert.assertEquals(calculator.subtraction(6, 3), 3, 0);
    }

    @Test
    public void checkMultiplication() {
        Assert.assertEquals(calculator.multiplication(6, 3), 18, 0);
    }

    @Test
    public void checkDivision() {
        Assert.assertEquals(calculator.division(6,0), Infinity, 0);
        Assert.assertEquals(calculator.division(6, 3), 6 / 3, 0);
        Assert.assertEquals(calculator.division(-18, 6), -3, 0);
    }

    @Test
    public void checkFactorial(){
        Assert.assertEquals(calculator.factorial(2), new BigInteger(Integer.toString(2)));
        Assert.assertEquals(calculator.factorial(0), new BigInteger(Integer.toString(1)));

    }

}
